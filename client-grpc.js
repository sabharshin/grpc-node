'use strict';
const fs = require('fs');

//Importing GRPC and the proto loader
const grpc = require('grpc');
const loader = require('@grpc/proto-loader');

//reads the proto
const packageDefinition = loader.loadSync('devices.proto', {
  keepCase: false,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
});

//Loads the proto file to be used in constant pkg
const pkg = grpc.loadPackageDefinition(packageDefinition);

//Creates server
const PORT = 9000;

//changes
const cacert = fs.readFileSync('certs/ca.crt'),
      cert = fs.readFileSync('certs/client.crt'),
      key = fs.readFileSync('certs/client.key'),
      kvpair = {
          'private_key': key,
          'cert_chain': cert
      };
const creds = grpc.credentials.createSsl(cacert, key, cert);

var options = {
  'grpc.ssl_target_name_override' : 'ca',
  'grpc.default_authority': 'ca'
};

//changes

const client = new pkg.DeviceService(`localhost:${PORT}`, creds, options);

//Reads command from server
//Use a number for switch option
const option = parseInt(process.argv[2],11);

switch (option) {
    case 1:
        //Shows the Metadata for user on server side
        sendMetadata(client);
        break;
    case 2:
        //Unary Call, gets device by id
        getByBadgeNumber(client);
        break;
    case 3:
        //Server streaming call where server sends all the devices
        getAll(client);
        break;
    case 4:
        //Client Streaming request where the client streams to the server
        addPhoto(client);
        break;
    case 5:
        //Bidirectional streaming
        //Stream request and stream response back
        saveAll(client);
        break;
}


function saveAll(client) {
    const devices = [
      {
        id: 4,
  			badgeNumber: 123,
  			name: "Device 3",
  			location: "Europe"
  		},
  		{
        id: 5,
  			badgeNumber: 234,
  			name: "Device 4",
  			location: "Sweden"
  		}
    ];


    //starts the call and open the channel to send and receive
    const call = client.saveAll();
    call.on('data', function (dev) {
        console.log(dev.device);
    });
    devices.forEach(function (dev) {
        call.write({device: dev});
    });
    //inform server to close call
    call.end();
}

function addPhoto(client) {
    const md = new grpc.Metadata();
    md.add('badgenumber', '2080');

    //creates client to send stream message accross (Photo)
    //return error if any or result if streaming is Ok
    const call = client.addPhoto(md, function (err, result) {
        console.log(result);
    });

    //uses fs to read image file
    const stream = fs.createReadStream('Penguins.jpg');
    //Chunk data to send it in different pieces
    stream.on('data', function (chunk) {
        call.write({data: chunk});
    });
    stream.on('end', function () {
        call.end();
    });
}


function getAll(client) {
    const call = client.getAll({});

    call.on('data', function (data) {
        console.log(data.device);
    });
}

function sendMetadata(client) {
    const md = new grpc.Metadata();
    md.add('username', 'mvansickle');
    md.add('password', 'password1');

    client.getByBadgeNumber({}, md, function () {});
}


function getByBadgeNumber(client) {
    client.getByBadgeNumber({badgeNumber: 2080}, function (err, response) {
        if (err) {
            console.log('err: ', err);
        } else {
            console.log('response: ', response);
        }
    });
}
