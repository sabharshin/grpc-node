'use strict';

const fs = require('fs');
const grpc = require('grpc');
const loader = require('@grpc/proto-loader');
const packageDefinition = loader.loadSync('devices.proto', {
  keepCase: false,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
});

const pkg = grpc.loadPackageDefinition(packageDefinition);

const devices = require('./devices').devices;

const PORT = 9000;

const cacert = fs.readFileSync('certs/ca.crt'),
      cert = fs.readFileSync('certs/server.crt'),
      key = fs.readFileSync('certs/server.key'),
      kvpair = {
          'private_key': key,
          'cert_chain': cert
      };
const creds = grpc.ServerCredentials.createSsl(cacert, [kvpair]);


//Creates the new server
const server = new grpc.Server();

//Services
server.addService(pkg.DeviceService.service, {
  getByBadgeNumber: getByBadgeNumber,
  getAll: getAll,
  addPhoto: addPhoto,
  saveAll: saveAll,
  save: save
});

server.bind(`localhost:${PORT}`, creds);
console.log(`Welcome, the server is running on port ${PORT}`);
//starts the server
server.start();

//Function on server side to verify batch id and send it to the client
function getByBadgeNumber(call, callback){

  const md = call.metadata.getMap();
    for (let key in md) {
        console.log(key, md[key]);
    }

  //pass batch numbr from client request to the constant badge number
  const badgeNumber = call.request.badgeNumber;
    for (let i = 0; i < devices.length; i++) {
        if (devices[i].badgeNumber === badgeNumber) {
            callback(null, {device: devices[i]});
            return;
        }
    }

  callback('error');
}


function getAll(call){
  devices.forEach(function(dev) {
    call.write({device: dev});
});

call.end();
}


function addPhoto(call, callback){
  const md = call.metadata.getMap();
    for (let key in md) {
        console.log(key, md[key]);
    }
    //buffers and add the pieces of the image
    let result = new Buffer(0);
    call.on('data', function (data) {
        result = Buffer.concat([result, data.data]);
        console.log(`Message received with size ${data.data.length}`);
    });

    //Returns the message informing if streaming was successful and inform size in bytes
    call.on('end', function() {
        callback(null, {isOk: true});
        console.log(`Total file size: ${result.length} bytes`);
    })
}

function saveAll(call, callback){
    call.on('data', function (dev) {
      devices.push(dev.device);
      call.write({device: dev.device});
  });
  call.on('end', function () {
      devices.forEach(function (dev) {
          console.log(dev);
      });
      call.end();
  });
}

//not in use
function save(call, callback){

}
