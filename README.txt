You must have nodejs to run this project. If you don't have it yet, you can install it from here:
https://nodejs.org/en/download/

The Main service is to manage employees holidays
This service consist in verifying user metadata (Username and password) and process the following:
- Unary Call, gets employee by id
- Server streaming call where server sends all the employees
- Client Streaming request where the client streams to the server
- Bidirectional streaming

1. To run this service first run the following comand line code from the folder firstService:
node server2

2. Then form the same folder, on a command line run the client and send one of the option parameters to process the client request calls:

1- Retrieve metadata    (sendMetadata)
2- Unary Call, gets employee by id    (getByBadgeNumber)
3- Server streaming call where server sends all the employees   (getAll)
4- Client Streaming request where the client streams to the server    (addPhoto)
5- Bidirectional streaming    (saveAll)

node client2 {number parameter}

example: node client2 1
