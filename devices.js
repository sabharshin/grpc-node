const devices = [
    {
		id: 1,
		badgeNumber: 2080,
		name: "Device 1",
		location: "Coimbatore"
	},
    {
		id: 2,
		badgeNumber: 7538,
		name: "Device  2",
		location: "USA"
	},
    {
		id: 3,
		badgeNumber: 5144,
		name: "Device 3",
		location: "Hydrebad"
	}
];

exports.devices = devices;
